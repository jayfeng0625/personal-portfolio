import React from 'react'
import { useLocation } from 'react-router-dom'

import { HeaderContainer, Header, ImageContainer, Image } from './styles'

const UserHeader = ({ user }) => {
  const location = useLocation()

  return (
    <HeaderContainer isHome={location.pathname === '/'}>
      <Header>
        <ImageContainer>
          <Image src={user.basics.picture}/>
        </ImageContainer>
        <div>
          <h2>{user.basics.name}</h2>
          <h4>
            <a
              href={`https://gitconnected.com/${user.basics.username}`}
              target="_blank"
              rel="noreferrer noopener"
            >
              @{user.basics.username}
            </a>
          </h4>
          <br />
          <p>{user.basics.label}</p>
          <p>Based in {user.basics.region}</p>
          <p>{user.basics.yearsOfExperience} years of experience in the industry</p>
          <br />
          <p>{user.basics.headline}</p>
        </div>
      </Header>
    </HeaderContainer>
  )
}

export default UserHeader

import styled from 'styled-components'

export const HeaderContainer = styled.div`
  display: flex;
  justify-content: space-between;

  @media (max-width: 640px) {
    display: ${({ isHome }) => (!isHome ? 'none' : 'flex')};
    flex-direction: column;
  }
`

export const Header = styled.div`
  display: flex;

  @media (max-width: 640px) {
    flex-direction: column;
  }
`

export const ImageContainer = styled.div`
  width: ${({ imageWidth }) => (!!imageWidth ? '500px' : imageWidth)};
  margin-right: 16px;
  border-radius: 2px;
`

export const Image = styled.img`
  display: block;
  width: 100%;
  height: auto;
`
